import json
import os
import shutil
from telegram import *
from telegram.ext import *


class User:
    filename = str("allUsers/")
    allObjects = []

    def addToFile(self):
        print("HERE")
        username = str(self.username)
        print(username)
        try:
            shutil.rmtree(User.filename + username)
        finally:
            os.makedirs(User.filename + username)
            f = open("ids", "a+")
            f.write(username + "\n")
            f.close()
            print("added to ids")
            f = open(User.filename + username + "/info", "w+")
            print(self.chatId)
            f.write(str(self.chatId))
            f.close()
            print("added to info")
            f = open(User.filename + username + "/lends", "w+")
            f.write(json.dumps(self.lendTo))
            f.close()

    def __init__(self, chatId, username):
        username = str(username.lower())
        self.chatId = chatId
        self.username = str(username.lower())
        print("registered", self.username)
        self.lendTo = {}
        self.addToFile()
        User.allObjects.append(self)

    @staticmethod
    def getByUsername(username):
        username = str(username.lower())
        for user in User.allObjects:
            if user.username == username:
                return user

    @staticmethod
    def isIn(username):
        username = username.lower()
        for user in User.allObjects:
            if user.username == username:
                return True
        return False

    @staticmethod
    def readAllData():
        print("HERE starting read")
        f = open("ids", "r")
        tmp = f.readlines()
        f.close()
        f = open("ids", "w+")
        f.close()

        print(tmp)
        for username in tmp:
            print(username)
            username = username[:-1]
            f = open(User.filename + username + "/info", "r")
            chatId = int(f.read())
            f.close()
            f = open(User.filename + username + "/lends", "r")
            print("reading lends")
            lendTo = {}
            xxx = f.read()
            f.close()
            print("WTF HERE")
            print(xxx)
            lendTo = json.loads(xxx)
            print(lendTo)

            user = User(chatId=chatId, username=username)
            f = open(User.filename + username + "/lends", "w")
            f.write(json.dumps(lendTo))
            user.lendTo = lendTo
            print(user.lendTo)
            f.close()

    def add(self, user, amount):
        print("ADDING")
        if user.username not in self.lendTo:
            self.lendTo[user.username] = amount
        else:
            self.lendTo[user.username] += amount

        print(self.lendTo)
        try:
            f = open(User.filename + self.username + "/lends", "w")
        except Exception as error:
            print(error)
        else:
            f.write(json.dumps(self.lendTo))
            f.close()


class Lend:
    allObjects = []

    def __init__(self, user1, user2, amount, senderId):
        self.amount = amount
        self.user1 = user1
        self.user2 = user2
        self.messageId = senderId
        self.senderId = senderId
        Lend.allObjects.append(self)

    @staticmethod
    def isThere(messageId):
        for l in Lend.allObjects:
            if l.messageId == messageId:
                return True

        return False

    @staticmethod
    def get(messageId):
        for l in Lend.allObjects:
            if l.messageId == messageId:
                return l

    def accept(self):
        Lend.allObjects.remove(self)
        print("accepting")
        print(self.amount)
        try:
            self.user1.add(self.user2, self.amount)
        except Exception as error:
            print(error)
        self.user2.add(self.user1, -self.amount)

    def reject(self):
        Lend.allObjects.remove(self)


def checkMember(update, context):
    print("OOMADAM INJA")
    if int(update.effective_chat.id) == 302763503:
        context.bot.send_message(chat_id=update.effective_chat.id, text="دلام قناری.")
    chatId = update.effective_chat.id
    if update.message.chat.username is None:
        context.bot.send_message(chat_id=chatId, text="برای استفاده از بات ما باید آیدی داشته باشید.")
        raise Exception()
    username = update.message.chat.username
    if not User.isIn(username):
        User(chatId=chatId, username=username)


def start(update, context):
    checkMember(update, context)
    content = open("start.txt", "r").read()
    chatId = update.effective_chat.id
    context.bot.send_message(chat_id=chatId, text=content)


def lend(update, context):
    checkMember(update, context)
    try:
        spliced = update.message.text.split()
        username1 = update.message.chat.username.lower()
        amount = float(spliced[2])
        username2 = spliced[1]
        if username1 == username2:
            raise Exception("equal users")
        print(username1, username2, amount)
        print(User.allObjects)
        if User.isIn(username2):
            user1 = User.getByUsername(username1)
            user2 = User.getByUsername(username2)
            l = Lend(user1, user2, amount, update.message.message_id)
            text = "کاربر "
            text += username1
            text += " ادعا میکند به شما مقدار "
            text += str(amount)
            text += " تومان پول قرض داده است.\nآیا تایید میکنید؟ جواب خود را در ریپلای به این مسیج به صورت بله یا خیر " \
                    "بگویید. "

            l.messageId = context.bot.send_message(chat_id=user2.chatId, text=text).message_id
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, text="کاربر مورد نظر در بات ما حاضر نیست.")
            raise Exception("user not found")
    except Exception as error:
        context.bot.send_message(chat_id=update.effective_chat.id, text="لطفن با فرمت درست دستور را وارد کنید.")
        print(error)
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text="درخواست ثبت قرض برای شخص مورد نظر فرستاده "
                                                                        "شد. منتظر تایید او بمانید.")


def helpMe(update, context):
    checkMember(update, context)
    content = open("help.txt", "r").read()
    idx = update.message.message_id
    context.bot.send_message(chat_id=update.effective_chat.id, text=content, reply_to_message_id=idx)


def handler(update, context):
    print("HERE")
    print(update)
    checkMember(update, context)
    print("NOW HERE")
    print(int(update.effective_chat.id))
    if False:
        context.bot.send_message(chat_id=update.effective_chat.id, text="دلام قناری.")
    else:
        idx = 0
        try:
            idx = update.message.reply_to_message.message_id
            if Lend.isThere(idx):
                if update.message.text == "بله" or update.message.text == "بده بکنیم":
                    messageId = update.message.reply_to_message.message_id
                    if Lend.isThere(messageId):
                        l = Lend.get(messageId)
                        context.bot.send_message(chat_id=l.user1.chatId, text="کاربر مورد نظر این قرض را تایید کرد.",
                                                 reply_to_message_id=l.senderId)
                        context.bot.send_message(chat_id=update.effective_chat.id, text="قرض مورد نظر تایید شد.")
                        l.accept()
                else:
                    if update.message.text == "خیر":
                        messageId = update.message.reply_to_message.message_id
                        if Lend.isThere(messageId):
                            l = Lend.get(messageId)
                            context.bot.send_message(chat_id=l.user1.chatId, text="کاربر مورد نظر این قرض را رد کرد.",
                                                     reply_to_message_id=l.senderId)
                            context.bot.send_message(chat_id=update.effective_chat.id, text="قرض مورد نظر رد شد.")
                            l.reject()
                    else:
                        context.bot.send_message(chat_id=update.effective_chat.id,
                                                 text="لطفن به فرمت بله یا خیر ریپلای کنید.")
        except:
            idx = -1


def balance(update, context):
    checkMember(update, context)
    chatId = update.effective_chat.id
    username = update.message.chat.username.lower()
    text = "حساب شما:\n"
    user = User.getByUsername(username)
    for l in user.lendTo:
        if user.lendTo[l] != 0:
            text += l + " " + str(user.lendTo[l]) + "\n"

    context.bot.send_message(chat_id=chatId, text=text)


def main():
    User.readAllData()
    updater = Updater(token='990602174:AAGnYTSxBc_pC9FNNmeXxzRvzgdZyntpyXY', use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('help', helpMe))
    dp.add_handler(CommandHandler('lend', lend))
    dp.add_handler(CommandHandler('balance', balance))
    dp.add_handler(MessageHandler(Filters.text, handler))
    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
